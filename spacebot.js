const Discord = require('discord.js');
const Logger = require('winston');
const NanoExpress = require('nanoexpress');
const Config = require('./config.json');
const fs = require('fs');

// Configure Logger settings
Logger.remove(Logger.transports.Console);
Logger.add(new Logger.transports.Console, {
    colorize: true
});

Logger.level = 'debug';

// Initialize Discord Bot
const DiscordBot = new Discord.Client();
DiscordBot.commands = new Discord.Collection();

// Load discord commands
const CommandFiles = fs.readdirSync('./discord/commands').filter(file => file.endsWith('.js'));
for (const File of CommandFiles) {
	const Command = require(`./discord/commands/${File}`);
	DiscordBot.commands.set(Command.Name, Command);
}

DiscordBot.once('ready', () => {
    Logger.info('Connected');
    Logger.info('Logged in as: ');
    Logger.info(DiscordBot.user.username + ' - (' + DiscordBot.user.id + ')');
});

let AvailableServers = new Map();
let AvailableServersBySession = new Map();
function ServerLogin(LoginKey)
{
    if (LoginKey in Config.ServerAuthentication.ValidKeys)
    {
        const ServerName = Config.ServerAuthentication.ValidKeys[LoginKey];
        const NewSessionKey = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        
        AvailableServers.set(ServerName, {
            SessionKey: NewSessionKey,
            MessageQueue: []
        });

        AvailableServersBySession.forEach((Value, Key) => {
            if (Value === ServerName)
            {
                AvailableServersBySession.delete(Key);
            }
        });

        AvailableServersBySession.set(NewSessionKey, ServerName);

        return {Success: true, SessionKey: NewSessionKey};
    }

    Logger.warn(`Someone tried to login with bad login key ${LoginKey}`);

    return {Success: false};
}

function QueueMessage(MessageSource, MessageText)
{
    AvailableServers.forEach((ServerObj, ServerName) => {
        if (MessageSource !== ServerName)
        {
            ServerObj.MessageQueue.push({
                Source: MessageSource,
                Message: MessageText
            });
        }
    });
}

DiscordBot.on('message', Message => {
    // Never respond to our own bot
    if (Message.author.bot)
    {
        return;
    }

    const ChannelID = Message.channel.id;

    // If the message starts with the command prefix then process it as a command
    if (Message.content.startsWith(Config.Discord.Prefix))
    {
        const Args = Message.content.slice(Config.Discord.Prefix.length).split(/ +/);
        const Command = Args.shift().toLowerCase();

        if (!DiscordBot.commands.has(Command))
        {
            return;
        }

        try
        {
            const BotCommand = DiscordBot.commands.get(Command);

            if (BotCommand.AdminOnly && ChannelID != Config.Discord.AdminChannel)
            {
                Logger.warn("User " + Message.author.username + " tried to run admin command in channel " + ChannelID);
                return;
            }

            if (ChannelID == Config.Discord.ChatLinkChannel || ChannelID == Config.Discord.AdminChannel)
            {
                BotCommand.Execute(Message, Args);
            }
            else
            {
                Logger.warn("User " + Message.author.username + " tried to run command in channel " + ChannelID);
            }
        } 
        catch (error)
        {
            Logger.error(error);
            Message.reply('there was an error trying to execute that command!');
        }
    }

    // Otherwise, queue the message to be relayed to any logged-in server sessions
    else if (ChannelID == Config.Discord.ChatLinkChannel)
    {
        QueueMessage("Discord", `${Message.author.username}: ${Message.cleanContent}`);
    }
});

DiscordBot.login(Config.Discord.Token);

// Options for HTTPS server
const HTTPSOptions = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

// Start HTTPS server to listen for incoming chat from servers
const HTTPServer = NanoExpress({
    https: {
        key_file_name: 'key.pem',
        cert_file_name: 'cert.pem'
    }
});

/**
 * Listens for incoming chat relay requests from servers
 * 
 * Parameters:
 * Session - The server's session key
 * User - The name of the chat user
 * Message - The user's message
 */
HTTPServer.post("/chat", 
    {
        schema: {
            body: {
                type: 'object',
                properties: {
                    session: {type: 'string'},
                    user: {type: 'string'},
                    message: {type: 'string'}
                },
                required: ['session', 'user', 'message']
            },
            response: false,
            query: false,
            params: false,
            cookies: false
        }
    },
    async (Request, Response) => {
        const UserName = Request.body.user;
        const UserMessage = Request.body.message;
        const SessionKey = Request.body.session;

        // validate key
        if (!AvailableServersBySession.has(SessionKey))
        {
            Response.status(401);
            Response.end();
            return;
        }

        const ServerName = AvailableServersBySession.get(SessionKey);
        const Channel = DiscordBot.channels.cache.get(Config.Discord.ChatLinkChannel);
        try
        {
            Channel.send(`**[${ServerName}]** ${UserName}: ${UserMessage}`);
            QueueMessage(ServerName, `${UserName}: ${UserMessage}`);
        }
        catch (Error)
        {
            Logger.error(Error);
            Response.status(500);
            Response.end();
            return;
        }

        Response.status(200);
        Response.end();
    }
);

/**
 * Listens for incoming event relay requests from servers
 * 
 * Parameters:
 * Session - The server's session key
 * Message - The server's message
 */
HTTPServer.post("/event", 
    {
        schema: {
            body: {
                type: 'object',
                properties: {
                    session: {type: 'string'},
                    message: {type: 'string'}
                },
                required: ['session', 'message']
            },
            response: false,
            query: false,
            params: false,
            cookies: false
        }
    },
    async (Request, Response) => {
        const Event = Request.body.message;
        const SessionKey = Request.body.session;

        // validate key
        if (!AvailableServersBySession.has(SessionKey))
        {
            Response.status(401);
            Response.end();
            return;
        }

        const ServerName = AvailableServersBySession.get(SessionKey);
        const Channel = DiscordBot.channels.cache.get(Config.Discord.ChatLinkChannel);
        try
        {
            Channel.send(`**[${ServerName}]** ${Event}`);
            QueueMessage(ServerName, `${Event}`);
        }
        catch (Error)
        {
            Logger.error(Error);
            Response.status(500);
            Response.end();
            return;
        }

        Response.status(200);
        Response.end();
    }
);

/**
 * Listens for incoming login requests from servers
 * 
 * Parameters:
 * key - The login key of the server
 * 
 * Returns:
 * session - The session key to be used with other API calls
 */
HTTPServer.post("/serverlogin", 
    {
        schema: {
            body: {
                type: 'object',
                properties: {
                    key: {type: 'string'}
                },
                required: ['key']
            },
            response: false,
            query: false,
            params: false,
            cookies: false
        }
    },
    async (Request, Response) => {
        Logger.info(`Server logging in with key ${Request.body.key}`);
        const LoginResult = ServerLogin(Request.body.key);
        if (LoginResult.Success)
        {
            Response.status(200);
            Response.setHeader("Content-Type", "application/json");
            Response.send(LoginResult);
            return;
        }

        Response.status(403);
        Response.end();
    }
);

/**
 * Provides servers with their queued chat messages
 * 
 * Parameters:
 * Session - The server's session key
 * 
 * Returns:
 * Messages - Array of queued messages for the server
 */
HTTPServer.get("/chatqueue", async (Request, Response) => {
    const SessionKey = Request.query.session;

    // validate key
    if (!AvailableServersBySession.has(SessionKey))
    {
        Response.status(401);
        Response.end();
        return;
    }

    const ServerName = AvailableServersBySession.get(SessionKey);
    const ServerData = AvailableServers.get(ServerName);
    if (ServerData == undefined)
    {
        Response.status(400);
        Response.end();
        return;
    }

    // if there are no messages then send nothing
    if (ServerData.MessageQueue.length <= 0)
    {
        Response.status(204);
        Response.end();
        return;
    }

    // clone the message queue for sending
    const Messages = ServerData.MessageQueue.splice(0, ServerData.MessageQueue.length);

    Response.status(200);
    Response.setHeader("Content-Type", "application/json");
    Response.send({Messages: Messages});
});

HTTPServer.listen(Config.HTTPS.Port, Config.HTTPS.BindIP);